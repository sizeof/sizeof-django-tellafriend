.. sizeof-django-tellafriend documentation master file, created by
   sphinx-quickstart on Thu Jul  3 00:16:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sizeof-django-tellafriend's documentation!
=====================================================

Contents:

.. toctree::
   :maxdepth: 2

    Prerequisites <prerequisites>
    Installation <installation>
    Configuration <configuration>
    Templates <templates>
    Changelog <changelog>
    License <license>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

